json.extract! update, :id, :title, :content, :created_at, :updated_at
json.url update_url(update, format: :json)
