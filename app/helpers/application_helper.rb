module ApplicationHelper

  # ページごとの完全なタイトルを返します。
  def full_title(page_title = '')
    base_title = "研究機材.com"
    if page_title.empty?
      "研究機材を" + base_title + "で共同購入・クラウドファンディング"
    else
      page_title + " | " + base_title
    end
  end
end
