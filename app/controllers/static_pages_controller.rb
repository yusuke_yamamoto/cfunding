class StaticPagesController < ApplicationController
  def home
  if logged_in?
    @review = current_user.reviews.build
    @feed_items = current_user.feed.paginate(page: params[:page])
  else
    @review = Review.new
  end
end


  def aboutus
    if logged_in?
      @review = current_user.reviews.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    else
      @review = Review.new
    end
  end

end
