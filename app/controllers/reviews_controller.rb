class ReviewsController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user,   only: :destroy
  # GET /reviews
  # GET /reviews.json
  def index
    @reviews = Review.paginate(page: params[:page])
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
    @review = Review.find(params[:id])
    @sum = Review.find(params[:id]).comments.sum(:price)
    @comments = @review.comments.paginate(page: params[:page])
    if logged_in?
      @comment = current_user.comments.build
    else
      @comment = Comment.new
    end
  end

  # GET /reviews/new
  def new
    @review = Review.new
  end

  # GET /reviews/1/edit
  def edit
    @review = Review.find(params[:id])
  end

  # POST /reviews
  # POST /reviews.json
    def create
      if Item.find_by(title: params[:review][:item_id])
        item_id = Item.find_by(title: params[:review][:item_id]).id
        params[:review][:item_id] = item_id
      else
        @item = Item.new
        @item.title = params[:review][:item_id]
        session[:review] = params[:review]
        flash[:warning] = "商品が存在しません。先に追加してください"
        render "items/new" and return
      end

      if logged_in? && params[:review][:user_id] != "5"
        @review = current_user.reviews.build(review_params)
      else
        @review = User.find(5).reviews.build(review_params)
      end

      if @review.save
        flash[:success] = "レビューが作成されました"
        redirect_to root_url
      else
        @feed_items = []
        render 'static_pages/home' and return
      end
    end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
    @review = Review.find(params[:id])
    if @review.update_attributes(review_params)
      flash[:success] = "レビューを更新しました"
      redirect_to @review
    else
      render 'edit'
    end
  end
  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review.destroy
    flash[:success] = "レビューを削除しました"
    redirect_to request.referrer || root_url
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:content,:user_id, :item_id, :point, :picture, :title, :payment)
    end

    def correct_user
      @review = current_user.reviews.find_by(id: params[:id])
      redirect_to root_url if @review.nil?
    end
end
