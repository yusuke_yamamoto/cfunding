class Review < ApplicationRecord
    belongs_to :user
    default_scope -> { order(created_at: :desc) }
    mount_uploader :picture, PictureUploader
    validates :user_id, presence: true
    validates :content, presence: true
    belongs_to :item
    validates :item_id,  presence: true
    validate  :picture_size
    validates :title, presence: true
    validates :point, presence: true
    validates :payment, presence: true
#    validates :point,  presence: true, inclusion: { in: %w(1,2,3,4,5), message: "1から5で評価してください" }
    has_many  :comments

private
  # アップロードされた画像のサイズをバリデーションする
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "5MB以下の画像にしてください")
    end
  end
end
