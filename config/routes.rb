Rails.application.routes.draw do
  get 'contact/index'
  resources :comments
  resources :types
  resources :categories
  root 'static_pages#home'
  get  '/aboutus',    to: 'static_pages#aboutus'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get 'contact',  to: 'contact#index'
  post 'contact/confirm',  to: 'contact#confirm'
  post 'contact/thanks',  to: 'contact#thanks'
  get   '/typeahead' => 'items#typeahead_action'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :items
  resources :updates
  resources :reviews
  resources :relationships,       only: [:create, :destroy]
end
