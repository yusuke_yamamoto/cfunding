crumb :root do
  link "Home", root_path
end

crumb :users do
  link 'ユーザー一覧', users_path
end

crumb :user do |user|
  link "#{user.name}", user_path(user)
  parent :users
end

crumb :types do
  link '専攻', types_path
end

crumb :type do |type|
  link "#{type.name}", type_path(type)
  parent :types
end

crumb :help do
  link 'ヘルプ', help_path
end

crumb :aboutus do
  link  "機材募集.comとは", aboutus_path
end

crumb :categories do
  link '機材カテゴリー', categories_path
end

crumb :category do |category|
  link "#{category.name}", category_path(category)
  parent :categories
end

crumb :items do
  link '機材', items_path
end

crumb :item do |item|
  link "#{item.title}", item_path(item)
  parent item.type
end


crumb :reviews do
  link '募集案件', reviews_path
end

crumb :review do |review|
  link "#{review.title}", review_path(review)
  parent review.item
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).
