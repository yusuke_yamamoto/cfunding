class AddPaymentToReviews < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :payment, :integer
  end
end
